import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Common{
    /**
     * Validate if input str is valid URL
     * @param url 
     * @returns boolean 
     */
    public validateUrl(url) {
        var pattern = new RegExp(
          "^(https?:\\/\\/)?" + // protocol
            "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
            "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
            "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
            "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
            "(\\#[-a-z\\d_]*)?$",
          "i"
        ); 
        
        return pattern.test(url);
      }
    
      /**
     * Get username from Torre URL-
     * @param url 
     * @returns username
     */
      public extractUsername(url) {
        var fields = url.split("/");
        return fields[fields.length - 1];
      }
    

}