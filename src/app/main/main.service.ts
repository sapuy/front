import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class MainService {
    constructor(private http: HttpClient) { }

    getBio(username) {
        return this.http.get(environment.apiUrl + 'bio/'+username).pipe(
            map(resp => {
                return resp;
            }),
            catchError(err => {
               
                  return throwError(err);
            })
        );
    }


   
}
