import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  AfterContentChecked,
} from "@angular/core";
import {
  NgbModal,
  NgbModalConfig,
  ModalDismissReasons,
} from "@ng-bootstrap/ng-bootstrap";
import * as Rellax from "rellax";
import { MainService } from "../main.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { Common } from '@shared/helpers/common';

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],

  providers: [NgbModalConfig, NgbModal],
})
export class ProfileComponent implements OnInit, AfterViewInit {
  @ViewChild("content") myModal: any;


  closeResult: string;
  buttonDisabled: boolean;
  public zoom: number = 12;
  public lat: number =51.678418;
  public lng: number = -7.809007;
  public styles: any[] = [
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [{ color: "#e9e9e9" }, { lightness: 17 }],
    },
    {
      featureType: "landscape",
      elementType: "geometry",
      stylers: [{ color: "#f5f5f5" }, { lightness: 20 }],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [{ color: "#ffffff" }, { lightness: 17 }],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [{ color: "#ffffff" }, { lightness: 29 }, { weight: 0.2 }],
    },
    {
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [{ color: "#ffffff" }, { lightness: 18 }],
    },
    {
      featureType: "road.local",
      elementType: "geometry",
      stylers: [{ color: "#ffffff" }, { lightness: 16 }],
    },
    {
      featureType: "poi",
      elementType: "geometry",
      stylers: [{ color: "#f5f5f5" }, { lightness: 21 }],
    },
    {
      featureType: "poi.park",
      elementType: "geometry",
      stylers: [{ color: "#dedede" }, { lightness: 21 }],
    },
    {
      elementType: "labels.text.stroke",
      stylers: [{ visibility: "on" }, { color: "#ffffff" }, { lightness: 16 }],
    },
    {
      elementType: "labels.text.fill",
      stylers: [{ saturation: 36 }, { color: "#333333" }, { lightness: 40 }],
    },
    { elementType: "labels.icon", stylers: [{ visibility: "off" }] },
    {
      featureType: "transit",
      elementType: "geometry",
      stylers: [{ color: "#f2f2f2" }, { lightness: 19 }],
    },
    {
      featureType: "administrative",
      elementType: "geometry.fill",
      stylers: [{ color: "#fefefe" }, { lightness: 20 }],
    },
    {
      featureType: "administrative",
      elementType: "geometry.stroke",
      stylers: [{ color: "#fefefe" }, { lightness: 17 }, { weight: 1.2 }],
    },
  ];
  data: Date = new Date();
  focus;
  focus1;

  //Profile Data
  public urlTorre;
  public person= {
    name: "",
    professionalHeadline: "",
    picture: "",
    summaryOfBio: "",
    weight: "",
    location:{
      name:'',
      country:''
    },
  };
  public links = {
    twitter: "",
    linkedin: "",
    website: "",
  };
  public stats = {
    strengths: "",
    awards: "",
    education: "",
    interests: "",
    jobs: "",
  };

  public jobs: any;
  public education: any;
  public awards: any;
  public strengths: any;
  public interests: any;
  public languages: any;

  constructor(
    private cdref: ChangeDetectorRef,
    config: NgbModalConfig,
    private modalService: NgbModal,
    private service: MainService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private elRef: ElementRef,
    private common: Common,
  ) {
    config.backdrop = "static";
    config.keyboard = false;
  }

  ngOnInit() {
    var rellaxHeader = new Rellax(".rellax-header");

    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
    var navbar = document.getElementsByTagName("nav")[0];
    navbar.classList.add("navbar-transparent");
    this.buttonDisabled = false;
  }
  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }
  ngAfterViewInit() {
    //alert('TRY');
    this.open(this.myModal);
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
    var navbar = document.getElementsByTagName("nav")[0];
    navbar.classList.remove("navbar-transparent");
  }
  open(content) {
    this.modalService.open(content).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
        if (this.common.validateUrl(this.urlTorre)) {
          this.spinner.show();
          let username = this.common.extractUsername(this.urlTorre);
          this.getBio(username);
          var btn=this.elRef.nativeElement.parentElement.querySelector(".nav-link");
          console.log("BTN",btn);
          btn.setAttribute('href','https://torre.co/api/genome/bios/'+username+'/export-resume');
        } else {
          this.open(this.myModal);
        }
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private updateData(data) {
    data = data.data;
    this.person = data.person;
    this.stats = data.stats;

    data.person.links.forEach((link) => {
      switch (link.name) {
        case "twitter":
          this.links.twitter = link.address;
          break;
        case "linkedin":
          this.links.linkedin = link.address;
          break;
        case "":
          this.links.website = link.address;
          break;
      }
    });
    this.jobs = data.jobs;
    this.awards = data.awards;
    this.education = data.education;
    this.strengths = data.strengths;
    this.interests = data.interests;
    this.languages = data.languages;

    this.lat=data.person.location.latitude;
    this.lng=data.person.location.longitude;

    this.spinner.hide();
  }
  private validateUrl() {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // fragment locator
    if (pattern.test(this.urlTorre)) {
      this.spinner.show();
      let username = this.extractUsername(this.urlTorre);
      this.getBio(username);
      var btn=this.elRef.nativeElement.parentElement.querySelector(".nav-link");
      console.log("BTN",btn);
      btn.setAttribute('href','https://torre.co/api/genome/bios/'+username+'/export-resume');
    } else {
      this.open(this.myModal);
    }
  }

  private extractUsername(url) {
    var fields = url.split("/");
    return fields[fields.length - 1];
  }

  private getBio(username) {
    this.service.getBio(username).subscribe(
      (data) => {
        console.log(data);
        this.updateData(data);
      },
      (error) => {
        this.spinner.hide();
        this.open(this.myModal);
        this.toastr.show("", `User not found, check the link and try again`, {
          timeOut: 5000,
          closeButton: true,
          enableHtml: true,
          toastClass: `alert alert-danger alert-with-icon`,
          positionClass: "toast-top-right",
        });
      }
    );
  }

  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
