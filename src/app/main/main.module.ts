import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';
import { NouisliderModule } from 'ng2-nouislider';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { AgmCoreModule } from '@agm/core';
import { ImageUploadModule } from '../shared/image-upload/image-upload.module';
import { MainComponent } from './main.component';
import { ProfileComponent } from './profile/profile.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        TagInputModule,
        HttpClientModule,
        NouisliderModule,
        JwBootstrapSwitchNg2Module,
        AngularMultiSelectModule,
        FormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBiXMzKB8mPGexb8PU-V86HWJlu4DPSI0A'
        }),
        ImageUploadModule,
        NgxSpinnerModule,
        ToastrModule.forRoot(),
    ],
    declarations: [
        MainComponent,
        ProfileComponent,
        ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MainModule { }
